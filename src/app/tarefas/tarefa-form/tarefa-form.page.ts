import { TarefaService } from './../shared/tarefa.service';
import { Tarefa } from './../shared/tarefa';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tarefa-form',
  templateUrl: './tarefa-form.page.html',
  styleUrls: ['./tarefa-form.page.scss'],
})
export class TarefaFormPage implements OnInit {
  tarefa: Tarefa;
  id: number = -1;
  titulo: string = '';

  constructor(private tarefaService: TarefaService, private route: ActivatedRoute, private router: Router,
    private toastController: ToastController) { }

  ngOnInit() {    
    this.tarefa = new Tarefa();
    this.titulo = 'Nova tarefa';

    let idParam = this.route.snapshot.paramMap.get('id');
    if (idParam) {
      this.titulo = 'Editando tarefa';
      this.id = parseInt(idParam);
      this.tarefaService.getByIndex(this.id)
        .then((tarefa: Tarefa) => {
          this.tarefa = tarefa;
        })
    }
  }

  removerFoto() {
    this.tarefa.foto = '';
  }

  onSubmit() {
    if (this.tarefa) {
      let result: Promise<{}>;
      let mensagem: string;

      if (this.id >= 0) {
        mensagem = 'Tarefa editada com sucesso';
        result = this.tarefaService.update(this.tarefa, this.id);
      } else {
        mensagem = 'Tarefa incluída com sucesso';
        result = this.tarefaService.insert(this.tarefa);
      }

      result.then(() => {
        this.toastController.create({
          message: mensagem,
          duration: 2000
        }).then((toast) => toast.present());

        this.router.navigate(['../']);
      }).catch(() => {
        this.toastController.create({
          message: 'Erro ao tentar salvar a tarefa',
          duration: 2000
        }).then((toast) => toast.present());
      })
    }
  }
}
