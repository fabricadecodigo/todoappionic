import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaFormPage } from './tarefa-form.page';

describe('TarefaFormPage', () => {
  let component: TarefaFormPage;
  let fixture: ComponentFixture<TarefaFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarefaFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
