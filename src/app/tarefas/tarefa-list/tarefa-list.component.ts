import { Component, OnInit, Input } from '@angular/core';
import { Tarefa } from '../shared/tarefa';
import { TarefaService } from '../shared/tarefa.service';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tarefa-list',
  templateUrl: './tarefa-list.component.html',
  styleUrls: ['./tarefa-list.component.scss'],
})
export class TarefaListComponent implements OnInit {

  @Input()
  tarefas: Tarefa[] = [];

  @Input()
  permitirConcluir: boolean = false;

  constructor(private tarefaService: TarefaService, private router: Router, private camera: Camera,
    private loadingController: LoadingController, private toastController: ToastController) { }

  ngOnInit() { }

  editar(tarefa: Tarefa) {
    this.tarefaService.getIndex(tarefa).then(index => {
      this.router.navigate(['/tarefas/editar/', index]);
    })
  }

  deletar(tarefa: Tarefa) {
    this.tarefaService.deteltar(tarefa).then(() => {
      this.removerDaLista(tarefa);
      this.toastController.create({
        message: 'Tarefa removida com sucesso.',
        duration: 2000
      }).then((toast) => toast.present());
    });
  }

  concluirTarefa(tarefa: Tarefa) {
    this.tarefaService.concluirTarefa(tarefa).then(() => {
      if (this.permitirConcluir) {
        this.toastController.create({
          message: 'Tarefa concluída com sucesso.',
          duration: 2000
        }).then((toast) => toast.present());
        this.removerDaLista(tarefa);
      }
    })
  }

  private removerDaLista(tarefa: Tarefa) {
    const index = this.tarefas.indexOf(tarefa);
    this.tarefas.splice(index, 1);
  }

  tirarFoto(tarefa: Tarefa) {
    this.showLoading();

    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.tarefaService.salvarFoto(tarefa, imageData)
        .then(() => {
          this.loadingController.dismiss();
          this.toastController.create({
            message: 'Foto salva com sucesso.',
            duration: 2000
          }).then((toast) => toast.present());
        })
        .catch(() => {
          this.loadingController.dismiss();
          this.toastController.create({
            message: 'Erro ao salvar a foto.',
            duration: 2000
          }).then((toast) => toast.present());
        });
    }, (err) => {
      console.error(err);
      this.loadingController.dismiss();
    });
  }

  async showLoading() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...'
    });
    loading.present();    
  }
}
