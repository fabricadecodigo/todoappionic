import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaListPage } from './tarefa-list.page';

describe('TarefaListPage', () => {
  let component: TarefaListPage;
  let fixture: ComponentFixture<TarefaListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarefaListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
