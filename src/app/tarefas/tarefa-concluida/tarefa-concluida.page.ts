import { Component, OnInit } from '@angular/core';
import { Tarefa } from '../shared/tarefa';
import { TarefaService } from '../shared/tarefa.service';

@Component({
  selector: 'app-tarefa-concluida',
  templateUrl: './tarefa-concluida.page.html',
  styleUrls: ['./tarefa-concluida.page.scss'],
})
export class TarefaConcluidaPage implements OnInit {
  tarefas: Tarefa[] = [];
  
  constructor(private tarefaService: TarefaService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.tarefaService.getTarefasConcluidas().then((tarefas: Tarefa[]) => {
      this.tarefas = tarefas;
    });
  }
}
