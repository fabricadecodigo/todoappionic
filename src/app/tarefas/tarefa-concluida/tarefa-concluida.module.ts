import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TarefaConcluidaPage } from './tarefa-concluida.page';
import { TarefaSharedModule } from '../shared/tarefa-shared.module';

const routes: Routes = [
  {
    path: '',
    component: TarefaConcluidaPage
  }
];

@NgModule({
  imports: [
    TarefaSharedModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TarefaConcluidaPage]
})
export class TarefaConcluidaPageModule {}
