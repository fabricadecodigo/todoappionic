import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaConcluidaPage } from './tarefa-concluida.page';

describe('TarefaConcluidaPage', () => {
  let component: TarefaConcluidaPage;
  let fixture: ComponentFixture<TarefaConcluidaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarefaConcluidaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaConcluidaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
