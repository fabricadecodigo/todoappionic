export class Tarefa {
    titulo: string = '';
    descricao: string = '';
    foto: string = '';
    concluida: boolean = false;
}
