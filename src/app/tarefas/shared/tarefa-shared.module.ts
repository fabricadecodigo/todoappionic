import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TarefaListComponent } from '../tarefa-list/tarefa-list.component';

@NgModule({
  declarations: [TarefaListComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    CommonModule,
    TarefaListComponent
  ]
})
export class TarefaSharedModule { }
