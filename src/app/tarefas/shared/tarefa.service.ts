import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Tarefa } from './tarefa';

@Injectable({
  providedIn: 'root'
})
export class TarefaService {
  private TAREFAS: string = 'tarefas';

  constructor(private storage: Storage) { }

  insert(tarefa: Tarefa) {
    return new Promise((resolve) => {
      this.getAll().then((tarefas: Tarefa[]) => {
        tarefas.push(tarefa);
        this.storage.set(this.TAREFAS, tarefas);
        resolve();
      });
    });
  }

  update(tarefa: Tarefa, index: number) {
    return new Promise((resolve) => {
      this.getAll().then((tarefas: Tarefa[]) => {
        tarefas[index] = tarefa;
        this.storage.set(this.TAREFAS, tarefas);
        resolve();
      });
    });    
  }

  private getAll() {
    return new Promise((resolve, reject) => {
      let tarefas: Tarefa[];

      this.storage.get(this.TAREFAS)
        .then((tarefasDb: Tarefa[]) => {
          if (tarefasDb && tarefasDb.length > 0) {
            tarefas = tarefasDb;
          } else {
            tarefas = [];
          }
          resolve(tarefas);
        })
        .catch(() => reject());
    });
  }

  getByIndex(index: number) {
    return this.getAll().then((tarefas: Tarefa[]) => {
      return tarefas[index];
    })
  }

  getTarefasEmAberto() {
    return this.getTarefas(false);
  }

  getTarefasConcluidas() {
    return this.getTarefas(true);
  }

  private getTarefas(concluida: Boolean) {
    return new Promise((resolve, reject) => {
      this.getAll()
        .then((tarefasDb: Tarefa[]) => {
          let tarefas = tarefasDb.filter(t => t.concluida == concluida);
          resolve(tarefas);
        })
        .catch(() => reject());
    });
  }

  getIndex(tarefa: Tarefa) {
    return this.getAll().then((tarefasDb: Tarefa[]) => {
      const index = tarefasDb.findIndex(t => 
        t.titulo == tarefa.titulo
        && t.descricao == tarefa.descricao 
        && t.concluida == tarefa.concluida);
      return index;
    })
  }

  deteltar(tarefa: Tarefa) {
    return new Promise((resolve, reject) => {
      this.getIndex(tarefa).then(index => {
        this.getAll().then((tarefasDb: Tarefa[]) => {
          tarefasDb.splice(index, 1);
          this.storage.set(this.TAREFAS, tarefasDb);
          resolve();
        }).catch(() => reject());
      }).catch(() => reject());
    });
  }

  concluirTarefa(tarefa: Tarefa) {
    return new Promise((resolve) => {
      this.getIndex(tarefa).then(index => {
        this.getAll().then((tarefasDb: Tarefa[]) => {
          tarefasDb[index].concluida = true;
          this.storage.set(this.TAREFAS, tarefasDb);
          resolve();
        })
      })
    });
  }

  salvarFoto(tarefa: Tarefa, foto: string) {
    return new Promise((resolve) => {
      this.getIndex(tarefa).then(index => {
        this.getAll().then((tarefasDb: Tarefa[]) => {
          tarefasDb[index].foto = foto;
          this.storage.set(this.TAREFAS, tarefasDb);
          resolve();
        })
      })
    });
  }
}
