import { Component, OnInit } from '@angular/core';
import { Tarefa } from '../shared/tarefa';
import { TarefaService } from '../shared/tarefa.service';

@Component({
  selector: 'app-tarefa-em-aberto',
  templateUrl: './tarefa-em-aberto.page.html',
  styleUrls: ['./tarefa-em-aberto.page.scss'],
})
export class TarefaEmAbertoPage implements OnInit {
  tarefas: Tarefa[] = [];

  constructor(private tarefaService: TarefaService) {
    // let tarefa1 = new Tarefa();
    // tarefa1.titulo = 'Titulo da tarefa 1'
    // tarefa1.descricao = 'Mussum Ipsum, cacilds vidis litro abertis. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis! Sapien in monti palavris qui num significa nadis i pareci latim. Delegadis gente finis, bibendum egestas augue arcu ut est. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis.'

    // let tarefa2 = new Tarefa();
    // tarefa2.titulo = 'Titulo da tarefa 1'
    // tarefa2.descricao = 'Mussum Ipsum, cacilds vidis litro abertis. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis! Sapien in monti palavris qui num significa nadis i pareci latim. Delegadis gente finis, bibendum egestas augue arcu ut est. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis.'

    // this.tarefas.push(tarefa1);
    // this.tarefas.push(tarefa2);    
  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.tarefaService.getTarefasEmAberto().then((tarefas: Tarefa[]) => {
      this.tarefas = tarefas;
    });
  }
}
