import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaEmAbertoPage } from './tarefa-em-aberto.page';

describe('TarefaEmAbertoPage', () => {
  let component: TarefaEmAbertoPage;
  let fixture: ComponentFixture<TarefaEmAbertoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarefaEmAbertoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaEmAbertoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
