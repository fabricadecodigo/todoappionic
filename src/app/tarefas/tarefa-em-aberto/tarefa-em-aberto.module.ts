import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TarefaEmAbertoPage } from './tarefa-em-aberto.page';
import { TarefaSharedModule } from '../shared/tarefa-shared.module';

const routes: Routes = [
  {
    path: '',
    component: TarefaEmAbertoPage
  }
];

@NgModule({
  imports: [
    TarefaSharedModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TarefaEmAbertoPage]
})
export class TarefaEmAbertoPageModule {}
