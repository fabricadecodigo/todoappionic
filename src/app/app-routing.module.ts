import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'naoconcluida',
    pathMatch: 'full'
  },
  // {
  //   path: 'home',
  //   loadChildren: './home/home.module#HomePageModule'
  // },
  // {
  //   path: 'list',
  //   loadChildren: './list/list.module#ListPageModule'
  // },
  {
    path: 'naoconcluida',
    loadChildren: './tarefas/tarefa-em-aberto/tarefa-em-aberto.module#TarefaEmAbertoPageModule'
  },
  {
    path: 'concluida',
    loadChildren: './tarefas/tarefa-concluida/tarefa-concluida.module#TarefaConcluidaPageModule'
  },
  {
    path: 'tarefas/nova',
    loadChildren: './tarefas/tarefa-form/tarefa-form.module#TarefaFormPageModule'
  },
  {
    path: 'tarefas/editar/:id',
    loadChildren: './tarefas/tarefa-form/tarefa-form.module#TarefaFormPageModule'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
